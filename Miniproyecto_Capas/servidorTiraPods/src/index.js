const { urlencoded, query } = require('express');

const express = require('express');

const morgan = require('morgan');

var cp = require('child_process');

const path = require('path');

const fetch = require("node-fetch");

const app = express();

//settings

app.set('port', process.env.PORT || 3000)

//middleware

app.use(morgan('dev'));

app.use(urlencoded({ extended: false }));

app.use(express.json());

app.use((req, res, next) => {
  res.append('Access-Control-Allow-Origin', ['*']);
  res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.append('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

//routes

app.get('/weatherservice/pods', (req, res) => {

  console.log(tirarPod());

});

//static fields

app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});

//functions

function tirarPod(){

  var command1 = 'kubectl get pods';

  var command2 = 'kubectl delete pod ';

  var pods = cp.execSync(command1).toString();

  var podlist = pods.split("\n");

  podlist.shift();

  podlist.forEach(element => {
    
    if(element.substr(0,18) == "express-deployment"){

      cp.execSync(command2+element.substr(0,35));

    }
  });
}
