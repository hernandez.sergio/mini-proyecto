$(function () {

    $('#botonCiudad').on('click', function () {

      ciudad = {'ciudad': $('#climaCiudad').val()};
      
        $.post('http://miniproyectovm.westeurope.cloudapp.azure.com/weatherservice/clima', ciudad, function(clima){

              if(clima.descripcion == "Nombre de ciudad incorrecto"){

                $("tbody").html('<tr><td>Nombre de ciudad incorrecto</td></tr>');

              }else{

                $("#ciudad").html("Clima de: "+clima.ciudad)

                $("tbody").html('<tr><td>Temperatura:</td><td>'+(clima.temperatura)+' °C</td></tr><tr><td>Sensacion termica:</td><td>'+(clima.sensacionTermica)+' °C</td></tr><tr><td>Temperatura maxima:</td><td>'+(clima.temperaturaMaxima)+' °C</td></tr><tr><td>Temperatura minima:</td><td>'+(clima.temperaturaMinima)+' °C</td></tr><tr><td>Presion atmosferica:</td><td>'+(clima.presionAtmosferica)+' hPa</td></tr><tr><td>Humedad:</td><td>'+(clima.humedad)+' %</td></tr><tr><td>Visibilidad:</td><td>'+(clima.visibilidad)+' m</td></tr><tr><td>Viento:</td><td>'+(clima.viento)+'</td></tr>');
  
                $('#html').css('background-image', "URL(../weather/img/"+clima.descripcion+".jpg)");

              }
    
            },'json')
    })
  
    $('#botonVolver').on('click', function () {
        
      window.location = ("/weather/index.html");
  
    })
  })
  
  
