$(function () {
    
    $(document).ready( function () {
    {
        
        $.getJSON('http://miniproyectovm.westeurope.cloudapp.azure.com/weatherservice/rendimiento',function(rendimiento){

            var mensajeDisco = rendimiento[0];

            mensajeDisco = mensajeDisco.split("\n");

            mensajeDisco.forEach(element => {
              
              if (element.substring(element.length-1,element.length)=="/"){

                mensajeDisco = element.substring(element.length-5,element.length-2);

              }

            });

            var mensajeCPU = rendimiento[1].replace('\n', '');

            var mensajeMemoria = rendimiento[2].substring((rendimiento[2]).indexOf(":")+1,(rendimiento[2]).indexOf("S"));

            for (let index = 0; index < 20; index++) {
              
              mensajeMemoria = mensajeMemoria.replaceAll("  "," ");
              
            }

            mensajeMemoria = mensajeMemoria.split(" ");

            mensajeMemoria.shift();

            mensajeMemoria.forEach(element => {
              
              $(".memoria").append("<td>"+element+"</td>");

            });

            $(".disco").append("<td>"+mensajeDisco+"</td>");

            $(".cpu").append("<td>"+mensajeCPU+"%</td>");

      })
    }
  })

  $('#botonVolver').on('click', function () {
        
    window.location = ("/weather/index.html");

  })

})
