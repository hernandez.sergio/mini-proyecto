$(function () {

  let tempData;

  let historico;

  let cities = [];

  let ctx;

  var timeFormat = 'DD/MM/YYYY';

  let config;

  window.onload = function () {

    $.getJSON('http://miniproyectovm.westeurope.cloudapp.azure.com/weatherservice/historico', function(h){

      historico = h;

      let repetido;

      historico.forEach(element => {

        $("tbody").append('<tr><td>'+(element.Temperatura)+'</td><td>'+(element.Sensacion_termica)+'</td><td>'+(element.Temperatura_maxima)+'</td><td>'+element.Temperatura_minima+'</td><td>'+element.Presion_atmosferica+'</td><td>'+element.Humedad+'</td><td>'+element.Visibilidad+'</td><td>'+element.Viento+'</td><td>'+element.Fecha_de_consulta+'</td></tr>');

        repetido = false;

        cities.forEach(element2 => {

            if(element.Ciudad == element2){

                repetido = true;

            }
        });

        if (repetido == false) {

          if((element.Ciudad == " ")||(element.Ciudad == "")){

          }else{

            cities.push(element.Ciudad)

            $("select").append('<option>'+element.Ciudad+'</option>');

            }
        }
      });
    })

    ctx = document.getElementById("canvas").getContext("2d");

  }

  $('#ciudad').on('change', function () {

    var ciudad =  $('#ciudad').val();

    tempData = [];

    $("tbody").html("<tr><td>Temperatura</td><td>Sensacion termica</td><td>Temperatura maxima</td><td>Temperatura minima</td><td>Presion atmosferica</td><td>Humedad</td><td>Visibilidad</td><td>Viento</td><td>Fecha de consulta</td></tr>");

    historico.forEach(element => {

        if (element.Ciudad == ciudad) {

            $("tbody").append('<tr><td>'+(element.Temperatura)+'</td><td>'+(element.Sensacion_termica)+'</td><td>'+(element.Temperatura_maxima)+'</td><td>'+element.Temperatura_minima+'</td><td>'+element.Presion_atmosferica+'</td><td>'+element.Humedad+'</td><td>'+element.Visibilidad+'</td><td>'+element.Viento+'</td><td>'+element.Fecha_de_consulta+'</td></tr>');

            tempData.push({x:convertDate(element.Fecha_de_consulta), y:element.Temperatura});

        }
    });

    config = {

        type:    'line',

        data:    {

            datasets: [

                {

                    label: "Temperaturas",

                    data: tempData,

                    fill: false,

                    borderColor: 'red'

                }
            ]
        },
        options: {
            responsive: true,
            scales:     {
                xAxes: [{
                    type:       "time",
                    time:       {
                        format: timeFormat,
                        tooltipFormat: 'll'
                    },
                    scaleLabel: {
                        display:     true,
                        labelString: 'Date'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display:     true,
                        labelString: 'value'
                    }
                }]
            }
        }
    };


    window.myLine = new Chart(ctx, config);

  })

    $('#botonVolver').on('click', function () {

      window.location = ("/weather/index.html");

    })

    function convertDate(inputFormat) {

        function pad(s) { return (s < 10) ? '0' + s : s; }

        var d = new Date(inputFormat)

        return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')

    }
})
