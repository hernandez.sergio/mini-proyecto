package net.codejava.javaee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class rendimiento
 */
@WebServlet("/rendimiento")
public class rendimiento extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public rendimiento() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                
    	Process disco = Runtime.getRuntime().exec("df -h | cut -d ' ' -f2-");
    	
    	Process cpu = Runtime.getRuntime().exec("top -n 1 -b | head -1 | cut -d':' -f4 | cut -d',' -f1");
    	
    	Process memoria = Runtime.getRuntime().exec("free -h");
    	
    	BufferedReader disco_ = new BufferedReader(new InputStreamReader (disco.getInputStream()));
    	
    	BufferedReader cpu_ = new BufferedReader(new InputStreamReader (cpu.getInputStream()));
    	
    	BufferedReader memoria_ = new BufferedReader(new InputStreamReader (memoria.getInputStream()));    	
    	
    	ArrayList<String> output = new ArrayList<String>();
    	
    	output.add(disco_.lines().collect(Collectors.joining()));
    	output.add(cpu_.lines().collect(Collectors.joining()));
    	output.add(memoria_.lines().collect(Collectors.joining()));
    	
    	Gson gson = new GsonBuilder().create();
    	
    	String jsonArray = gson.toJson(output);
    	
    	PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.write(jsonArray);
        out.flush();
        out.close();
    	
    }

}

