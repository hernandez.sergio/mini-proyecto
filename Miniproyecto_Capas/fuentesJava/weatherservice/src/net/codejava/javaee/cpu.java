package net.codejava.javaee;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class cpu
 */
@WebServlet("/cpu")
public class cpu extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public cpu() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            	
    	Process update = Runtime.getRuntime().exec("apt-get update");
    	
    	Process install = Runtime.getRuntime().exec("apt-get install stress");
    	
    	Process stress = Runtime.getRuntime().exec("stress --cpu 2 -t 300");
    	
    }

}

