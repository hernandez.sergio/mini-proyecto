package net.codejava.javaee;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
/**
 * Servlet implementation class clima
 */
@WebServlet("/clima")
public class clima extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public clima() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        clsConsulta clima = null;
		try {
			clima = analizarClima(request.getParameter("ciudad"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

        almacenar(clima);
        
        Gson gson = new Gson();    
        String json = gson.toJson(clima);
          
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.write(json);
        out.flush();

    }
    
    private clsConsulta analizarClima(Object c) throws IOException, JSONException{
    	
    	String ciudad = c.toString();
        
        StringBuilder str = new StringBuilder();
        
        str.append("http://api.openweathermap.org/data/2.5/weather?q=");
        
        str.append(ciudad);
        
        str.append(",ES&APPID=b3b466e491d411e468b1edded8289d14");
        
        String url = str.toString();
        
        JSONObject json = readJsonFromUrl(url);
        
        JSONObject wind = (JSONObject) json.get("wind");
        
        JSONObject main = (JSONObject) json.get("main");
        
        JSONArray w =  (JSONArray) json.get("weather");
        
        JSONObject weather = (JSONObject) w.get(0);
        
        StringBuilder str1 = new StringBuilder();
        
        str1.append(wind.get("speed"));
        
        str1.append("km/h direccion ");
        
        str1.append(direccion((int) wind.get("deg")));
          
        clsConsulta clima = new clsConsulta(
                ((BigDecimal) main.get("temp")).doubleValue()-273.15, 
                ((BigDecimal) main.get("feels_like")).doubleValue() - 273.15, 
                ((BigDecimal)main.get("temp_max")).doubleValue()-273.15, 
                ((BigDecimal)main.get("temp_min")).doubleValue()-273.15, 
                (int)main.get("pressure"), 
                (int)main.get("humidity"), 
                (int)json.get("visibility"), 
                str1.toString(),  
                (String) weather.get("description"), 
                ciudad);
         
        return clima;
          
      }

    
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
          sb.append((char) cp);
        }
        return sb.toString();
      }

      public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
          BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
          String jsonText = readAll(rd);
          JSONObject json = new JSONObject(jsonText);
          return json;
        } finally {
          is.close();
        }
      }

      public String direccion(double grados){
          
          if ((grados>=338)&&(grados<=360)||(grados>0)&&(grados<=22)) {

            return "N";
            
          } else {
            if ((grados>=68)&&(grados<=112)) {

              return "E";
            
            } else {
              if ((grados>=158)&&(grados<=202)) {

                return "S";
            
              } else {
                if ((grados>=248)&&(grados<=292)) {

                  return "O";
            
                } else {
                  if ((grados>22)&&(grados<68)) {

                    return "NE";
            
                  } else {
                    if ((grados>112)&&(grados<158)) {

                      return "SE";
            
                    } else {
                      if ((grados>202)&&(grados<248)) {

                        return "SO";
            
                      } else {
                        if ((grados>292)&&(grados<338)) {

                          return "NO";
            
                        } else {
                          
                          return "No valida";

                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      
      public void almacenar(clsConsulta consulta) {
          
          MongoClient mongoClient = new MongoClient("historico", 27017);
          
          DB db = mongoClient.getDB("historicoclima");

          DBCollection collection = db.getCollection("consultas");
          
          collection.insert(consulta.toDBObjectConsulta());
          
      }
}


