package net.codejava.javaee;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

/**
 * Servlet implementation class historico
 */
@WebServlet("/historico")
public class historico extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public historico() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    	PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
			out.print(leer());
		} catch (JSONException e) {
			e.printStackTrace();
		}
        out.flush();
    }

    public JSONArray leer() throws JSONException{
        
        MongoClient mongoClient = new MongoClient("historico", 27017);
        
        DB db = mongoClient.getDB("historicoclima");

        DBCollection collection = db.getCollection("consultas");
        
        DBCursor cursor = collection.find();

        JSONArray jsonarray = new JSONArray();
    	
    	while(cursor.hasNext()) {
    		
    	    BasicDBObject obj = (BasicDBObject) cursor.next();
    	    
    	    JSONObject jsonobj = new JSONObject();
    	     
    	    jsonobj.put("temperatura", obj.getDouble("temperatura"));
    	    jsonobj.put("sensacionTermica", obj.getDouble("sensacionTermica"));
    	    jsonobj.put("temperaturaMaxima", obj.getDouble("temperaturaMaxima"));
    	    jsonobj.put("temperaturaMinima", obj.getDouble("temperaturaMinima"));
    	    jsonobj.put("presionAtmosferica", obj.getDouble("presionAtmosferica"));
    	    jsonobj.put("humedad", obj.getDouble("humedad"));
    	    jsonobj.put("visibilidad", obj.getDouble("visibilidad"));
    	    jsonobj.put("viento", obj.getString("viento"));
    	    jsonobj.put("descripcion", obj.getString("descripcion"));
    	    jsonobj.put("ciudad", obj.getString("ciudad"));
    	      	    
    	    jsonarray.put(jsonobj);
    	  }
    	
    	return jsonarray;
    	  
    }
}
