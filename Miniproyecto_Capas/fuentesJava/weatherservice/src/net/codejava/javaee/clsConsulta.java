package net.codejava.javaee;

import com.mongodb.BasicDBObject;

public class clsConsulta {

    double temperatura;
    double sensacionTermica;
    double temperaturaMaxima ;
    double temperaturaMinima;
    double presionAtmosferica;
    double humedad;
    double visibilidad;
    String viento;
    String descripcion;
    String ciudad;
    
    public clsConsulta(double temperatura, double sensacionTermica, double temperaturaMaxima, double temperaturaMinima,
            double presionAtmosferica, double humedad, double visibilidad, String viento, String descripcion,
            String ciudad) {
        super();
        this.temperatura = temperatura;
        this.sensacionTermica = sensacionTermica;
        this.temperaturaMaxima = temperaturaMaxima;
        this.temperaturaMinima = temperaturaMinima;
        this.presionAtmosferica = presionAtmosferica;
        this.humedad = humedad;
        this.visibilidad = visibilidad;
        this.viento = viento;
        this.descripcion = descripcion;
        this.ciudad = ciudad;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getSensacionTermica() {
        return sensacionTermica;
    }

    public void setSensacionTermica(double sensacionTermica) {
        this.sensacionTermica = sensacionTermica;
    }

    public double getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(double temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    public double getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public double getPresionAtmosferica() {
        return presionAtmosferica;
    }

    public void setPresionAtmosferica(double presionAtmosferica) {
        this.presionAtmosferica = presionAtmosferica;
    }

    public double getHumedad() {
        return humedad;
    }

    public void setHumedad(double humedad) {
        this.humedad = humedad;
    }

    public double getVisibilidad() {
        return visibilidad;
    }

    public void setVisibilidad(double visibilidad) {
        this.visibilidad = visibilidad;
    }

    public String getViento() {
        return viento;
    }

    public void setViento(String viento) {
        this.viento = viento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    public BasicDBObject toDBObjectConsulta() {

        BasicDBObject dBObjectFutbolista = new BasicDBObject();

        dBObjectFutbolista.append("temperatura", this.getTemperatura());
        dBObjectFutbolista.append("sensacionTermica", this.getSensacionTermica());
        dBObjectFutbolista.append("temperaturaMaxima", this.getTemperaturaMaxima());
        dBObjectFutbolista.append("temperaturaMinima", this.getTemperaturaMinima());
        dBObjectFutbolista.append("presionAtmosferica", this.getPresionAtmosferica());
        dBObjectFutbolista.append("humedad", this.getHumedad());
        dBObjectFutbolista.append("visibilidad", this.getVisibilidad());
        dBObjectFutbolista.append("viento", this.getViento());
        dBObjectFutbolista.append("descripcion", this.getDescripcion());
        dBObjectFutbolista.append("ciudad", this.getCiudad());

        return dBObjectFutbolista;
    }
}


